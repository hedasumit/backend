"use strict";
var ObjectId = require('mongoose').Types.ObjectId,
    User = require('../models/user.model.js').User,
    tokenService = require('../utilities/jwt.token.service.js'),
    apiCodes = require('../../config/api-codes.js');

module.exports = function (app) {


    app.getUsers = function (req, res) {
        User.find().exec(function (err, users) {
            if (err) {
                return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Users not Found'});
            }
            if (!users) {
                return res.status(apiCodes.BAD_REQUEST).send({message: 'Users not Available'});

            }
            else {
                return res.status(apiCodes.SUCCESS).send({data: users});
            }
        });
    };

    app.createUser = function (req, res) {
        if (req.body.email) {
            User.findOne({email: req.body.email}).exec(function (err, user) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Users not Found'});
                }
                else if (user) {
                    return res.status(apiCodes.DUPLICATE).send({message: 'User Already Exit'});

                }
                else {
                    var newUser = new User();
                    newUser.email = req.body.email;
                    newUser.name = req.body.name;
                    if (req.body.password) {
                        newUser.password = newUser.generateHash(req.body.password);
                    }
                    newUser.save(function (err, data) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in creating user'});
                        }
                        if (data) {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'User Created'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({message: 'Email Required'});
        }
    };


    app.getUserById = function (req, res) {
        return res.status(apiCodes.SUCCESS).send({
            data: req.userObject
        });
    };


    app.updateUserById = function (req, res) {
        if (req.body) {
            var user = req.userObject;
            user.email = req.body.email;
            user.name = req.body.name;
            user.save(function (err, response) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({
                        message: 'Error in updating user'
                    });
                } else {
                    return res.status(apiCodes.SUCCESS).send({
                        message: 'User updated'
                    });
                }
            })
        }
    };
    app.deleteUserById = function (req, res) {
        if (req.body) {
            var user = req.userObject;
            user.remove(function (err, response) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({
                        message: 'Error in deleting user'
                    });
                } else {
                    return res.status(apiCodes.SUCCESS).send({
                        message: 'User Deleted'
                    });
                }
            })
        }
    };

    app.getUserFromId = function (req, res, next) {
        if (req.params.userId) {
            User.findById(req.params.userId).exec(function (err, userObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting user'});
                }
                else if (!userObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Users not Available'});
                }
                req.userObject = userObject;
                next();
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'User id Not provided'
            });
        }

    };

    app.signin = function (req, res) {
        if (!req.body.email && !req.body.password) {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Email Not provided'
            });
        }
        else {
            User.findOne({
                'email': req.body.email
            }).exec(function (err, user) {
                if (err) {
                    return res.status(apiCodes.BAD_REQUEST).send({
                        message: 'Error in getting user'
                    });
                }

                // if no user is found, return the message
                if (!user) {
                    return res.status(apiCodes.BAD_REQUEST).send({
                        message: 'User Not Found'
                    });
                }
                if (req.body.password) {
                    if (!user.validPassword(req.body.password)) {
                        return res.status(apiCodes.UNAUTHORIZED).send({
                            message: 'Erong Password'
                        });
                    }
                    else {
                        delete user.password;
                        var resultUser = user.toObject();
                        var tokenizeObj = {id: resultUser._id, email: resultUser.email, role: resultUser.role};
                        res.jsonp({token: tokenService.issueToken(tokenizeObj)});
                    }
                }
                else {
                    return res.status(apiCodes.UNAUTHORIZED).send({
                        message: 'Enter Password'
                    });
                }


            });

        }
    };

};