/**
 * routes related to user.
 */
'use strict'
module.exports = function (app) {
    /* Authentication Rest api */
    app.route('/api/v1/users').get(app.ensureAuthenticated, app.getUsers)
        .post(app.createUser);
    app.route('/api/v1/user/:userId').get(app.ensureAuthenticated, app.getUserById)
        .put(app.ensureAuthenticated, app.updateUserById)
        .delete(app.ensureAuthenticated, app.deleteUserById);
    app.route('/api/v1/auth/login').post(app.signin);
    app.param('userId', app.getUserFromId);
}