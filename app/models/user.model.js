var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcryptjs');

var userSchema = Schema({
    role: {
        type: String,
        default: 'user'
    },
    password: {type: String},
    email: {type: String},
    name: {type: String}

});

// Password encryption

userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
};

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

var User = mongoose.model('User', userSchema)
module.exports.User = User;
