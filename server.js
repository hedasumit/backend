/**
 * to start the project run node sever.js
 * this is main server file
 */

//load required files
const express = require('express'),
    app = express(),
    fs = require('fs'),
    bodyParser = require('body-parser'),
    dbConfig = require('./config/database.js'),
    mongoose = require('mongoose');
var cors = require('cors');
//define maximum connections
//define application port
var port = 2000;


// Display the test page at '/'
app.use(express.static('public'));
// Database connection
mongoose.connect(dbConfig.url);

// Middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());


// load all controller =========================================================
var controller_loc = __dirname + '/app/controllers';
var controller_files = fs.readdirSync(controller_loc);
controller_files.forEach(function (file) {
    return (require(controller_loc + '/' + file))(app);
});
//load auth middlewares
require('./app/middleware/auth.middileware.js')(app);

// load all routes =========================================================
var routes_loc = __dirname + '/app/routes';
var routes_files = fs.readdirSync(routes_loc);
routes_files.forEach(function (file) {
    return (require(routes_loc + '/' + file))(app);
});

// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:9000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.listen(port, function () {
    console.log('Express server listening on port ' + port);
})

  